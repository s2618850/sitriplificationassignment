from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import XSD
import pandas as pd
import sys

# Define namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")

# Create a Graph
g = Graph()
g.bind("saref", SAREF)
g.bind("ex", EX)

def csv_to_rdf(csv_file_path):
    df = pd.read_csv(csv_file_path)
    total_rows = df.shape[0]
    i = 0
    # Iterate over the rows in the df
    for index, row in df.iterrows():
        timestamp = row['utc_timestamp']  
        
        # per row, check the measurements per device 
        for column in df.columns:
            if column not in ['utc_timestamp', 'cet_cest_timestamp','interpolated']:  
                device_id = URIRef(EX[column])  
                measurement_id = URIRef(EX[f"Measurement_{column}_{index}"])
                
                # if device had not been created, it should be created
                if (device_id, RDF.type, SAREF.Device) not in g:
                    g.add((device_id, RDF.type, SAREF.Device))
                
                # Create measurement entity
                g.add((measurement_id, RDF.type, SAREF.Measurement))
                g.add((measurement_id, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                
                # Add value if available
                if pd.notna(row[column]):
                    g.add((measurement_id, SAREF.hasValue, Literal(row[column], datatype=XSD.float)))
                # else:
                #     g.add((measurement_id, SAREF.hasValue, Literal("missing", datatype=XSD.string)))
                
                # Missing values are excluded because this unnecessarily increases the size of the graph, also were missing data interpolated, so no NaN are 
                # expected for the devices which are active for a household. 

                # Link measurement to the device
                g.add((device_id, SAREF.makesMeasurement, measurement_id))
        if (index % 1000 == 0):
            print(f"Processed {index}/{total_rows} rows", end="\r")
        
        # for testing purposes: print the first 10 rows      
        # i+= 1
        
        # if (i > 10000):
        #     break

if __name__ == "__main__":
    if len(sys.argv) != 2:
        csv_file_path = "household_data_60min_singleindex.csv"  
    else:
        csv_file_path = sys.argv[1]
        
    csv_to_rdf(csv_file_path)
    
    try:
        print("Starting serialization... Serialization might take some minutes.")
        g.serialize(destination='graph.ttl', format='turtle')
        print("Serialization complete. File saved as 'graph.ttl'")
    except Exception as e:
        print("Error during serialization:", str(e))
